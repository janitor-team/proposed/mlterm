Source: mlterm
Section: x11
Priority: optional
Maintainer: أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@users.sourceforge.net>
Uploaders: Hideki Yamane <henrich@debian.org>
Build-Depends: debhelper-compat (= 12), libgtk-3-dev, libtool, libx11-dev, libxext-dev, libxft-dev, x11proto-core-dev, libfribidi-dev, libxrender-dev, libuim-dev (>= 1.4.1), libm17n-dev, libscim-dev, libgcroots-dev, libxml2-dev, libthai-dev, libibus-1.0-dev, fcitx-libs-dev, libssh2-1-dev, libcairo2-dev, libwnn-dev, libcanna1g-dev, libskk-dev
Standards-Version: 4.5.0
Rules-Requires-Root: binary-targets
Homepage: http://mlterm.sourceforge.net
Vcs-Git: https://salsa.debian.org/debian/mlterm.git
Vcs-Browser: https://salsa.debian.org/debian/mlterm

Package: mlterm
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Recommends: mlterm-tools
Suggests: unifont, xfonts-efont-unicode, fonts-vlgothic | fonts-japanese-gothic, fonts-noto-cjk | fonts-nanum, fonts-arphic-bsmi00lp, fonts-arphic-gbsn00lp, fonts-freefont-ttf, t1-cyrillic, mlterm-im-uim, mlterm-im-m17nlib, mlterm-im-scim
Conflicts: mlterm-tiny
Provides: x-terminal-emulator
Description: MultiLingual TERMinal
 This is a terminal emulator for X Window System, which supports
 various encodings including ISO-8859-[1-11,13-16], TCVN5712, VISCII,
 TIS-620 (same as ISO-8859-11), KOI8-{R,U,T}, CP{1251,1255}, GEORGEAN-PS,
 EUC-JP, EUC-JISX0213, ISO-2022-JP{,1,2,3}, Shift_JIS, Shift_JISX0213,
 ISO-2022-KR, EUC-KR, UHC, JOHAB, EUC-CN (aka GB2312), GBK, ISO-2022-CN,
 Big5, EUC-TW, HZ, UTF-8, and GB18030.
 .
 Doublewidth characters for east Asian, combining characters for
 Thai, Vietnamese, and other diacritics, BiDi (bi-directionality)
 for Arabic and Hebrew as well as Arabic shaping are all supported.
 Though mlterm supports Indic complex languages such as Hindi, this
 Debian package is not compiled with Indic support.
 .
 Since mlterm checks the current locale and selects appropriate encoding,
 you don't need to configure mlterm to display your language and
 encoding.
 .
 mlterm also supports unique features such as scrollbar API,
 multiple windows, multiple XIM, anti-alias using FreeType and Xft,
 and so on.

Package: mlterm-tiny
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Suggests: mlterm-tools, unifont, xfonts-efont-unicode, fonts-vlgothic | fonts-japanese-gothic, fonts-nanum | fonts-baekmuk, fonts-arphic-bsmi00lp, fonts-arphic-gbsn00lp, fonts-freefont-ttf, t1-cyrillic, mlterm-im-uim
Conflicts: mlterm
Provides: x-terminal-emulator
Description: MultiLingual TERMinal, tiny version
 This is a terminal emulator for X Window System, which supports
 various encodings including ISO-8859-[1-11,13-16], TCVN5712, VISCII,
 TIS-620 (same as ISO-8859-11), KOI8-{R,U,T}, CP{1251,1255}, GEORGEAN-PS,
 EUC-JP, EUC-JISX0213, ISO-2022-JP{,1,2,3}, Shift_JIS, Shift_JISX0213,
 ISO-2022-KR, EUC-KR, UHC, JOHAB, EUC-CN (aka GB2312), GBK, ISO-2022-CN,
 Big5, EUC-TW, HZ, UTF-8, and GB18030.
 .
 Doublewidth characters for east Asian, combining characters for
 Thai, Vietnamese, and other diacritics, BiDi (bi-directionality)
 for Arabic and Hebrew as well as Arabic shaping are all supported.
 Though mlterm supports Indic complex languages such as Hindi, this
 Debian package is not compiled with Indic support.
 .
 Since mlterm checks the current locale and selects appropriate encoding,
 you don't need to configure mlterm to display your language and
 encoding.
 .
 mlterm also supports unique features such as scrollbar API,
 multiple windows, multiple XIM, anti-alias using FreeType and Xft,
 and so on.
 .
 This tiny version does not support fancy features such as background
 image and so on, except for i18n-related features.

Package: mlterm-common
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, ncurses-term (>= 5.7+20101128-1)
Pre-Depends: ${misc:Pre-Depends}
Replaces: mlterm (<= 2.8.0.cvs20040403-2)
Recommends: mlterm | mlterm-tiny
Description: MultiLingual TERMinal, common files
 mlterm is a terminal emulator for X Window System, which supports
 various encodings, doublewidth characters, BiDi, Arabic shaping,
 and so on.
 .
 This package contains necessary files which are common for mlterm
 and mlterm-tiny packages.

Package: mlterm-tools
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, mlterm (= ${binary:Version}) | mlterm-tiny (= ${binary:Version})
Breaks: mlterm (<= 2.8.0.cvs20040403-2)
Replaces: mlterm (<= 2.8.0.cvs20040403-2)
Description: MultiLingual TERMinal, additional tools
 mlterm is a terminal emulator for X Window System, which supports
 various encodings, doublewidth characters, BiDi, Arabic shaping,
 and so on.
 .
 This package contains configuration tools and so on for mlterm.

Package: mlterm-im-uim
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, mlterm (= ${binary:Version}) | mlterm-tiny (= ${binary:Version})
Pre-Depends: ${misc:Pre-Depends}
Description: MultiLingual TERMinal, uim input method plugin
 mlterm is a terminal emulator for X Window System, which supports
 various encodings, doublewidth characters, BiDi, Arabic shaping,
 and so on.
 .
 This package contains uim input method plugin for mlterm.

Package: mlterm-im-scim
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, mlterm (= ${binary:Version}) | mlterm-tiny (= ${binary:Version})
Pre-Depends: ${misc:Pre-Depends}
Description: MultiLingual TERMinal, scim input method plugin
 mlterm is a terminal emulator for X Window System, which supports
 various encodings, doublewidth characters, BiDi, Arabic shaping,
 and so on.
 .
 This package contains scim input method plugin for mlterm.

Package: mlterm-im-m17nlib
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, mlterm (= ${binary:Version}) | mlterm-tiny (= ${binary:Version})
Pre-Depends: ${misc:Pre-Depends}
Description: MultiLingual TERMinal, m17nlib input method plugin
 mlterm is a terminal emulator for X Window System, which supports
 various encodings, doublewidth characters, BiDi, Arabic shaping,
 and so on.
 .
 This package contains m17nlib input method plugin for mlterm.

Package: mlterm-im-ibus
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, mlterm (= ${binary:Version}) | mlterm-tiny (= ${binary:Version})
Pre-Depends: ${misc:Pre-Depends}
Description: MultiLingual TERMinal, IBus input method plugin
 mlterm is a terminal emulator for X Window System, which supports
 various encodings, doublewidth characters, BiDi, Arabic shaping,
 and so on.
 .
 This package contains IBus input method plugin for mlterm.

Package: mlterm-im-fcitx
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, mlterm (= ${binary:Version}) | mlterm-tiny (= ${binary:Version})
Pre-Depends: ${misc:Pre-Depends}
Description: MultiLingual TERMinal, Flexible Input Method plugin
 mlterm is a terminal emulator for X Window System, which supports
 various encodings, doublewidth characters, BiDi, Arabic shaping,
 and so on.
 .
 This package contains Flexible Input Method plugin for mlterm.

Package: mlterm-im-wnn
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, mlterm (= ${binary:Version}) | mlterm-tiny (= ${binary:Version})
Pre-Depends: ${misc:Pre-Depends}
Description: MultiLingual TERMinal, FreeWnn input method plugin
 mlterm is a terminal emulator for X Window System, which supports
 various encodings, doublewidth characters, BiDi, Arabic shaping,
 and so on.
 .
 This package contains FreeWnn Input Method plugin for mlterm.

Package: mlterm-im-canna
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, mlterm (= ${binary:Version}) | mlterm-tiny (= ${binary:Version})
Pre-Depends: ${misc:Pre-Depends}
Description: MultiLingual TERMinal, Canna input method plugin
 mlterm is a terminal emulator for X Window System, which supports
 various encodings, doublewidth characters, BiDi, Arabic shaping,
 and so on.
 .
 This package contains Canna Input Method plugin for mlterm.

Package: mlterm-im-skk
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, mlterm (= ${binary:Version}) | mlterm-tiny (= ${binary:Version})
Pre-Depends: ${misc:Pre-Depends}
Description: MultiLingual TERMinal, SKK input method plugin
 mlterm is a terminal emulator for X Window System, which supports
 various encodings, doublewidth characters, BiDi, Arabic shaping,
 and so on.
 .
 This package contains Simple Kana Kanji (SKK) Input Method plugin for mlterm.
